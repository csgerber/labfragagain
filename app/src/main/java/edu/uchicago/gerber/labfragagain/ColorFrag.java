package edu.uchicago.gerber.labfragagain;


import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


public class ColorFrag extends Fragment {


    public static final String CONTENT = "CONTENT";
    public static final String COLOR = "COLOR";

    private ColorFrag.OnFragmentInteractionListener mListener;

    public ColorFrag() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_color, container, false);
        TextView textView = view.findViewById(R.id.content);
        String text = getArguments().getString(CONTENT, "default value");
        int color = getArguments().getInt(COLOR, 9879);
        final Button button = view.findViewById(R.id.button);
        button.setText(text);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mListener.onFragmentInteraction(mListener.toString() + " says, you just clicked " + button.getText());

            }
        });

        textView.setText(text);
        view.setBackgroundColor( color);
        return view;

    }

    public static ColorFrag newInstance(String text, int color){

        Bundle bundle = new Bundle();
        bundle.putString(CONTENT, text);
        bundle.putInt(COLOR, color);

        ColorFrag colorFrag = new ColorFrag();
        colorFrag.setArguments(bundle);
        return colorFrag;

    }

    public static ColorFrag newInstance(String text){


        return ColorFrag.newInstance(text, Color.parseColor("#12345678"));

    }

    public static ColorFrag newInstance(){

        return ColorFrag.newInstance("dummy text here");

    }



    //this interface is to communicate to the host Activity which will implement this interface
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(String message);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ColorFrag.OnFragmentInteractionListener) {
            mListener = (ColorFrag.OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }


}
