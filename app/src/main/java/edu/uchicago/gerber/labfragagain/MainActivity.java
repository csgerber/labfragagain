package edu.uchicago.gerber.labfragagain;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

import edu.uchicago.gerber.labfragagain.dummy.DummyContent;


public class MainActivity extends AppCompatActivity
        implements ColorFrag.OnFragmentInteractionListener,
                   ItemFragment.OnListFragmentInteractionListener
{


    public static final String FRAG_ORDER_KEY = "FRAG_ORDER_KEY";
    private Map map;
    private int key = R.id.navigation_home;

    private ColorFrag fragNotifs;
    private ItemFragment fragDash;
    private ColorFrag fragHome;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fragNotifs = ColorFrag.newInstance("Notifs text here.");
        fragDash = ItemFragment.newInstance(1);
        fragHome = ColorFrag.newInstance("Home text here", Color.parseColor("#33882200"));

        map = new HashMap();
        map.put(R.id.navigation_home, fragHome);
        map.put(R.id.navigation_dashboard, fragDash);
        map.put(R.id.navigation_notifications, fragNotifs);




        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {

                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                   setFragFromMenu(item.getItemId());
                   return true;
                }

        });
    }//end onCreate()


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(FRAG_ORDER_KEY, key);
        super.onSaveInstanceState(outState);

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {


        key = savedInstanceState.getInt(FRAG_ORDER_KEY, R.id.navigation_home);
        setFragFromMenu(key);

        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void onResume() {
        setFragFromMenu(key);

        super.onResume();
    }

    private void setFragFromMenu(int id) {
        key = id;
        swapInFragment((Fragment)map.get(id), R.id.container);


    }

    private void swapInFragment(Fragment fragment, int containerId){
        FragmentTransaction t = getSupportFragmentManager()
                .beginTransaction();

        t.replace(containerId, fragment);
        t.commit();
    }


    @Override
    public void onFragmentInteraction(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onListFragmentInteraction(DummyContent.DummyItem item) {
        Toast.makeText(this, item.content, Toast.LENGTH_LONG).show();
    }
}
